---- PREVIAMENTE SE CARGARON LAS ARCHIVOS PORPORCIONADOS

select * from proceso.tasas_productos;

select * from proceso.punto3_1_3;

-- CREAR TABLA QUE IDENTIFIQUE EL TIPO DE TASA DEL PRODUCTO Y SE LO ASIGNE A CADA UNO
CREATE TABLE proceso.punto3_1_3 STORED AS PARQUET AS
WITH definetasa AS (
    SELECT ob.radicado,
    ob.num_documento,
    ob.cod_segm_tasa,
    ob.cod_subsegm_tasa,
    ob.cal_interna_tasa,
    ob.id_producto,
    ob.tipo_id_producto,
    ob.fecha_desembolso,
    ob.valor_inicial,
    ob.plazo,
    ob.cod_periodicidad,
    ob.periodicidad,
    ob.saldo_deuda,
    ob.modalidad,
    ob.tipo_plazo,
    CASE
        WHEN lower(ob.id_producto) LIKE '%cartera%' then ts.tasa_cartera
        WHEN lower(ob.id_producto) LIKE '%leasing%' then ts.tasa_leasing
        WHEN lower(ob.id_producto) LIKE '%tarjeta%' then ts.tasa_tarjeta
        WHEN lower(ob.id_producto) LIKE '%sufi%' then ts.tasa_sufi
        WHEN lower(ob.id_producto) LIKE '%factoring%' then ts.tasa_factoring
        WHEN lower(ob.id_producto) LIKE '%hipotecario%' then ts.tasa_hipotecario
        WHEN lower(ob.id_producto) LIKE '%operacion_especifica%' then ts.tasa_operacion_especifica
        ELSE 0
    END as tpa
    FROM proceso.obligaciones_clientes ob
    LEFT JOIN proceso.tasas_productos ts ON ob.cod_segm_tasa = cod_segmento AND ob.cod_subsegm_tasa = ts.cod_subsegmento and ob.cal_interna_tasa = ts.calificacion_riesgos
)
SELECT radicado,
num_documento,
cod_segm_tasa,
cod_subsegm_tasa,
cal_interna_tasa,
id_producto,
tipo_id_producto,
fecha_desembolso,
valor_inicial,
plazo,
cod_periodicidad,
periodicidad,
saldo_deuda,
modalidad,
tipo_plazo,
tpa tasa_producto_asignado,
(((pow((1+tpa),(1/cod_periodicidad))-1)*cod_periodicidad)/cod_periodicidad) as tasa_efectiva,
((((pow((1+tpa),(1/cod_periodicidad))-1)*cod_periodicidad)/cod_periodicidad) * valor_inicial) as valor_final
FROM definetasa;

select * from proceso.punto3_1_4;

CREATE TABLE proceso.punto3_1_4 STORED AS PARQUET AS
SELECT num_documento, sum(valor_final) as total_obligaciones
FROM proceso.punto3_1_3
GROUP BY num_documento;


 


---------------------------------------------------------------------------------------
---
---------------        PARTE 3.2
---
---------------------------------------------------------------------------------------

--- 3.2.a
SELECT cedula, nombre
FROM cliente;

--- 3.2.b
SELECT c.cedula, c.nombre c.region
FROM cliente c
WHERE lower(c.region) = 'centro';

--- 3.2.c - CLIENTES CON MAS DE 3 CUENTAS ACTIVAS
WITH clientes_ctactiv as (
    SELECT cedula_cliente, count(cedula_cliente) cuentas
    FROM cuentas
    WHERE estado = 1 --- Activa
)
SELECT ct.cedula_cliente, cl.nombre, ct.num_cuenta, ct.estado
FROM cuentas ct
LEFT JOIN cliete cl ON ct.cedula_cliente = ct.dedula
LEFT JOIN clientes_ctactiv ca ON ct.cedula_cliente = ca.cedula_cliente
WHERE ca.cuentas > 3;


--- 3.2.d - SOLO NOMBRE DE CLIENTES CON CLAVE DINAMICA
SELECT cl.nombre
FROM cliente cl
INNER JOIN clave_dinamica cd ON cl.cedula = cd.cedula_cliente;

-- 3.2.e - CLIENTES QUE NO TIENEN CLAVE DINAMICA
SELECT cl.cedula, cl.nombre, cl.region
FROM cliente cl
INNER JOIN clave_dinamica cd ON cl.cedula = cd.cedula_cliente
WHERE cd.cedula_cliente IS NULL;

-- 3.2.f  - SALDO AGRUPADO DE LAS CUENTAS AGRUPADO POR REGION
SELECT cl.region, sum(ct.saldo) as saldo_region
FROM cuentas ct
LEFT JOIN cliete cl ON ct.cedula_cliente = ct.dedula
WHERE ca.estado = 1 --- creo que se calcula sobre las cuentas activas
GROUP BY cl.region
;

-- 3.2.g  - SALDO DE CUENTAS ACTIVAS ABIERTAS EN MAYO 2018 CUYOS CLIENTES TENGAN CLAVE DINAMICA

SELECT ct.num_cuenta, ct.saldo
FROM cuentas ct 
LEFT JOIN clave_dinamica cd ON ct.cedula_cliente = cd.cedula_cliente
WHERE ct.estado = 1
AND strleft(cast(ct.fecha_apertura as STRING),6) = "201805";

select * from proceso_vspc_finanzas.mov_r
where strleft(cast(f_doc_mov as STRING),6) = "202205";
